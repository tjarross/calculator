#include "calculator.h"

struct termios term, old_term;

static int	getch(void)
{
	int c = 0;

	tcgetattr(0, &old_term);
	memcpy(&term, &old_term, sizeof(term));
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	tcsetattr(0, TCSANOW, &term);
	c = getchar();
	tcsetattr(0, TCSANOW, &old_term);
	return (c);
}

static int	kbhit(void)
{
	int		c = 0;

	tcgetattr(0, &old_term);
	memcpy(&term, &old_term, sizeof(term));
	term.c_lflag &= ~(ICANON | ECHO);
	term.c_cc[VMIN] = 0;
	term.c_cc[VTIME] = 1;
	tcsetattr(0, TCSANOW, &term);
	c = getchar();
	tcsetattr(0, TCSANOW, &old_term);
	if (c != -1)
		ungetc(c, stdin);
	return ((c != -1) ? 1 : 0);
}

static int	kbesc(void)
{
	int		c;

	if (!kbhit())
		return (KEY_ESCAPE);
	c = getch();
	if (c == '[')
	{
		switch (getch())
		{
			case 'A':
				c = KEY_UP;
				break;
			case 'B':
				c = KEY_DOWN;
				break;
			case 'C':
				c = KEY_LEFT;
				break;
			case 'D':
				c = KEY_RIGHT;
				break;
			default:
				c = 0;
				break;
		}
	}
	else
		c = 0;
	if (c == 0)
		while (kbhit())
			getch();
	return (c);
}

static int	kbget(void)
{
	int c;

	c = getch();
	return (c == KEY_ESCAPE) ? kbesc() : c;
}

static void	move_cursor_forward(int i)
{
	printf("\033[%dD", i);
}

static void	move_cursor_backward(int i)
{
	printf("\033[%dC", i);
}

int			process_keys(std::string& input_line, unsigned int& cursor_index)
{
	int c = kbget();

	if (c == KEY_LEFT && cursor_index > 0)
	{
		--cursor_index;
		move_cursor_backward(1);
	}
	else if (c == KEY_RIGHT && cursor_index < input_line.length())
	{
		++cursor_index;
		move_cursor_forward(1);
	}
	else if (c == KEY_ENTER)
	{
		std::cout << std::endl;
		return (KEY_ENTER);
	}
	else if (isprint(c) || c == KEY_BACKSPACE)
	{
		if (cursor_index == 0 && c != KEY_BACKSPACE)
		{
			input_line += static_cast<char>(c);
			std::cout << static_cast<char>(c);
		}
		else
		{
			printf("\033[2K");
			if (c == 127)
			{
				if (cursor_index < input_line.length())
					input_line.erase(input_line.length() - cursor_index - 1, 1);
			}
			else
				input_line.insert(input_line.length() - cursor_index, reinterpret_cast<char *>(&c), 1);
			printf("\r>>> %s", input_line.c_str());
			if (cursor_index != 0 &&
				(cursor_index < input_line.length() || input_line.length() != 0))
				move_cursor_forward(cursor_index);
		}
	}
	return (0);
}
