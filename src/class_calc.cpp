#include "calculator.h"
#include "class_calc.h"

Calc::Calc() :
	equation_(""),
	refactored_(""),
	solutions_()
{

}

Calc::~Calc()
{

}

void	Calc::set_equation(const char *str)
{
	equation_ = std::string(str);
}

bool	Calc::check_equation_validity()
{
	return (true);
}

void	Calc::invalid_syntax()
{

}

void	Calc::refactor()
{

}

double	Calc::get_result(const std::vector<std::string> opt, const std::vector<double> opr)
{
	if (opt[0] == "**")
		return (std::pow(opr[0], opr[1]));
	if (opt[0] == "*")
		return (opr[0] * opr[1]);
	if (opt[0] == "/")
		return (opr[0] / opr[1]);
	if (opt[0] == "%")
		return (std::fmod(opr[0], opr[1]));
	if (opt[0] == "+")
		return (opr[0] + opr[1]);
	if (opt[0] == "-")
		return (opr[0] - opr[1]);
	if (opt[0] == "<<")
		return ((int)opr[0] << (int)opr[1]);
	if (opt[0] == ">>")
		return ((int)opr[0] >> (int)opr[1]);
	if (opt[0] == "<")
		return (opr[0] < opr[1]);
	if (opt[0] == "<=")
		return (opr[0] <= opr[1]);
	if (opt[0] == ">")
		return (opr[0] > opr[1]);
	if (opt[0] == ">=")
		return (opr[0] >= opr[1]);
	if (opt[0] == "==")
		return (opr[0] == opr[1]);
	if (opt[0] == "!=")
		return (opr[0] != opr[1]);
	if (opt[0] == "&")
		return ((int)opr[0] & (int)opr[1]);
	if (opt[0] == "^")
		return ((int)opr[0] ^ (int)opr[1]);
	if (opt[0] == "|")
		return ((int)opr[0] | (int)opr[1]);
	if (opt[0] == "&&")
		return (opr[0] && opr[1]);
	if (opt[0] == "||")
		return (opr[0] || opr[1]);
	return (0);
}

int     	is_opt(const char *str, const char *opt_list)
{
	char		oprt[MAX_OPERATOR_LEN + 1];
	int			j = 0;

	for (int i = 0; opt_list[i]; ++i)
	{
		if (opt_list[i] != ' ')
		{
			j = 0;
			std::memset(oprt, 0, MAX_OPERATOR_LEN + 1);
			while (opt_list[i] != ' ' && opt_list[i])
			{
				oprt[j++] = opt_list[i];
				++i;
			}
			if (std::strcmp(str, oprt) == 0)
				return (1);
			if (opt_list[i] == 0)
				break ;
		}
	}
	return (0);
}

void    Calc::reduce_parenthesis()
{
	int i = 0;
	while (equation_[i])
	{
		if (equation_[i] == '(' || equation_[i] == '[')
		{
			std::string sub_eq;
			int j = ++i;
			int eq_len = 0;
			int parenthesis = -1;
			int sq_bracket = -1;

			if (equation_[i - 1] == '(')
				parenthesis = 1;
			else
				sq_bracket = 1;
			while (equation_[j])
			{
				if (equation_[j] == '(')
					parenthesis++;
				if (equation_[j] == '[')
					sq_bracket++;
				if (equation_[j] == ')' || equation_[i] == ']')
				{
					if (equation_[i - 1] == '(')
						--parenthesis;
					else
						--sq_bracket;
					if (parenthesis == 0 || sq_bracket == 0)
						break ;
				}
				++j;
			}
			eq_len = j - i;
			sub_eq = std::string(&equation_[i], eq_len);
			Calc sub;

			sub.set_equation(sub_eq.c_str());
			sub.refactor();
			std::string res = sub.solve();
			equation_.erase(i - 1, eq_len + 2);
			equation_.insert(i - 1, res);
			i = 0;
			continue;
		}
		++i;
	}
}

double		Calc::get_number(const char *str)
{
	int		base = 10;
	int		i = 0;
	if (std::strlen(str) > 2 && std::strncmp(str, "0b", 2) == 0)
		base = 2;
	else if (std::strlen(str) > 2 && std::strncmp(str, "0x", 2) == 0)
		base = 16;
	else if (str[0] == '0' && std::strcmp(str, "0"))
		base = 8;
	if (base == 2 || base == 16)
		i = 2;
	else if (base == 8)
		i = 1;
	if (std::strchr(str, '.') == NULL)
	{
		try
		{
			long long value = std::stoll(std::string(&str[i]), nullptr, base);
			return (value);
		}
		catch (std::exception&)
		{
			return (0);
		}
	}
	return (std::atof(str));
}

void		Calc::evaluate_expr(const int i)
{
	std::vector<double>			opr;
	std::vector<std::string>	opt;
	char						oprt[MAX_OPERATOR_LEN + 1];

	std::memset(oprt, 0, MAX_OPERATOR_LEN + 1);
	opt.push_back(operation_[i]);
	opr.push_back(get_number(operation_[i - 1].c_str()));
	opr.push_back(get_number(operation_[i + 1].c_str()));
	std::string res_str = std::to_string(get_result(opt, opr));
	double res_value = std::atof(res_str.c_str());
	double round_eps = std::abs(res_value) - static_cast<int>(std::abs(res_value));
	if (round_eps > 1 - ROUND_EPSILON)
	{
		if (res_value >= 0.0)
			res_str = std::to_string(std::ceil(res_value));
		else
			res_str = std::to_string(std::floor(res_value));
	}
	else if (round_eps != 0.0 && round_eps < ROUND_EPSILON)
	{
		if (res_value >= 0.0)
			res_str = std::to_string(std::floor(res_value));
		else
			res_str = std::to_string(std::ceil(res_value));
	}
	operation_.erase(operation_.begin() + i - 1, operation_.begin() + i + 2);
	operation_.insert(operation_.begin() + i - 1, res_str);
	opt.clear();
	opr.clear();
}

void		Calc::exec_priorities(const char *opt_list)
{
	char oprt[MAX_OPERATOR_LEN + 1];

	if (operation_.size() < 3)
		return ;
	for (unsigned int i = 0; i < operation_.size(); ++i)
	{
		std::memset(oprt, 0, MAX_OPERATOR_LEN + 1);
		if (is_opt(operation_[i].c_str(), opt_list))
		{
			evaluate_expr(i);
			i = 0;
		}
	}
}

void		Calc::split_whitespaces()
{
	std::stringstream			ss(equation_);
	std::string					token;

	operation_.clear();
	while (std::getline(ss, token, ' '))
		operation_.push_back(token);
}

std::string	Calc::solve()
{
	std::vector<double>			opr;
	std::vector<std::string>	opt;

	reduce_parenthesis();
	split_whitespaces();
	exec_priorities(PRIORITY_CLASS_1);
	exec_priorities(PRIORITY_CLASS_2);
	exec_priorities(PRIORITY_CLASS_3);
	exec_priorities(PRIORITY_CLASS_4);
	exec_priorities(PRIORITY_CLASS_5);
	exec_priorities(PRIORITY_CLASS_6);
	exec_priorities(PRIORITY_CLASS_7);
	exec_priorities(PRIORITY_CLASS_8);
	exec_priorities(PRIORITY_CLASS_9);
	exec_priorities(PRIORITY_CLASS_10);
	exec_priorities(PRIORITY_CLASS_11);
	return (operation_[0]);
}

std::string		get_binary_number(double number)
{
	std::string bin = std::bitset<64>(number).to_string();
	char *begin = std::strchr(&bin[0], '1');
	return (begin == NULL ? std::string("0") : std::string(begin));
}

void    		Calc::print_result()
{
	char	sci[64] = {'\0'};
	char	dec[64] = {'\0'};
	char	hex[64] = {'\0'};
	char	oct[64] = {'\0'};
	double	number = get_number(operation_[0].c_str());

	snprintf(sci, 64, "%e", number);
	snprintf(dec, 64, "%lf", number);
	snprintf(hex, 64, "%x", (unsigned int)number);
	snprintf(oct, 64, "%o", (unsigned int)number);
	set_equation(dec);
	std::cout << "decimal     = " << dec << std::endl;
	std::cout << "hexadecimal = " << hex << std::endl;
	std::cout << "binary      = " << get_binary_number(number) << std::endl;
	std::cout << "octal       = " << oct << std::endl;
	std::cout << "scientific  = " << sci << std::endl;
}

void    Calc::print_equation()
{
	std::cout << equation_ << std::endl;
}
