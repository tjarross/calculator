#include "calculator.h"
#include "class_calc.h"

static void	resolve_arguments(int nb_args, char **args)
{
	Calc			calc;
	unsigned int	i = 0;

	while ((int)++i < nb_args)
	{
		calc.set_equation(args[i]);
		calc.print_equation();
		if (!calc.check_equation_validity())
		{
			calc.invalid_syntax();
			continue;
		}
		calc.refactor();
		calc.solve();
		calc.print_result();
		std::cout << std::endl;
	}
}

static void	console_mode()
{
	Calc			calc;
	unsigned int	cursor_index = 0;
	std::string		input_line = "";

	std::cout << ">>> ";
	while (1)
	{
		if (process_keys(input_line, cursor_index) == KEY_ENTER)
		{
			calc.set_equation(input_line.c_str());
			if (!calc.check_equation_validity())
			{
				calc.invalid_syntax();
				continue;
			}
			calc.refactor();
			calc.solve();
			calc.print_result();
			printf("\033[%luD\n", 4 + input_line.length());
			std::cout << ">>> ";
			cursor_index = 0;
			input_line = "";
		}
	}
}

int			main(int ac, char **av)
{
	if (ac > 1)
		resolve_arguments(ac, av);
	else
		console_mode();
	return (0);
}
