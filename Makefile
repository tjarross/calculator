# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/03/03 15:43:49 by tjarross          #+#    #+#              #
#    Updated: 2017/09/06 23:01:21 by tjarross         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

EXEC =		calculator
SRC_NAME =	main cursor_keys class_calc
SRC =  		$(addprefix src/, $(SRC_NAME:=.cpp))
OBJ =		$(SRC:%.cpp=%.o)
INC =		include/calculator.h
CC =		g++
FLAGS =		-g -std=c++11 -Wall -Wextra -I ./include
LIB =		-lncurses

all: $(EXEC)

msg:
	@echo "\033[0;29mMaking Project : \c"

$(EXEC): msg $(OBJ)
	@$(CC) -o $(EXEC) $(OBJ) $(LIB)
	@echo "\n\033[0;34mExecutable created !\033[0;29m"

test:
	@make -C tests/
	@./tests/test_calculator
	@cat test.log

src/%.o: src/%.cpp $(INC)
	@$(CC) $(FLAGS) -o $@ -c $<
	@echo "\033[0;32m.\c\033[0;29m"

clean:
	@echo "\033[0;31mCleanning Project Objects...\033[0;29m"
	@rm -f $(OBJ)

fclean: clean
	@echo "\033[0;31mCleanning Project Executable...\033[0;29m"
	@rm -f $(EXEC)

re: fclean all

.PHONY: all clean fclean re msg
