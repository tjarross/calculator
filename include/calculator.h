#ifndef CALCULATOR_H
# define CALCULATOR_H

# include <iostream>
# include <vector>
# include <cstring>
# include <cmath>
# include <sstream>
# include <bitset>

# include <termios.h>

# define CALCULATOR_VERSION "v0.1"

# define KEY_ESCAPE  	0x001b
# define KEY_ENTER   	0x000a
# define KEY_UP      	0x0105
# define KEY_DOWN    	0x0106
# define KEY_LEFT    	0x0108
# define KEY_RIGHT   	0x0107
# define KEY_BACKSPACE	0x007f

int		process_keys(std::string& input_line, unsigned int& cursor_index);

#endif
