#ifndef CLASS_CALC_H
# define CLASS_CALC_H

# include "calculator.h"

# define PRIORITY_CLASS_1	"**"
# define PRIORITY_CLASS_2	"* / %"
# define PRIORITY_CLASS_3	"+ -"
# define PRIORITY_CLASS_4	"<< >>"
# define PRIORITY_CLASS_5	"< <= > >="
# define PRIORITY_CLASS_6	"== !="
# define PRIORITY_CLASS_7	"&"
# define PRIORITY_CLASS_8	"^"
# define PRIORITY_CLASS_9	"|"
# define PRIORITY_CLASS_10	"&&"
# define PRIORITY_CLASS_11	"||"

# define MAX_OPERATOR_LEN	2

# define ROUND_EPSILON		0.0001

class Calc
{
	public:
		Calc();
		~Calc();

		void	    set_equation(const char *str);
		bool	    check_equation_validity();
		void	    invalid_syntax();
		void	    refactor();
		void        skip_spaces(std::string str, unsigned int &i);
		std::string	solve();
		void        print_result();
		double         get_result(std::vector<std::string> opt, std::vector<double> opr);
		void        reduce_parenthesis();
		void        print_equation();
		std::string get_rpn(std::string str);
		void		evaluate_expr(int i);
		void		exec_priorities(const char *opt_list);
		void		split_whitespaces();
		double		get_number(const char *str);

	private:
		std::string					equation_;
		std::string					refactored_;
		std::vector<float>          solutions_;
		std::vector<std::string>    operation_;
};

#endif
