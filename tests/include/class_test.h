#ifndef CLASS_TEST_H
# define CLASS_TEST_H

#include <string>

#include <stdio.h>

class Test
{
	public:
		Test();
		~Test();

		void	set_base(int value);
		void	is_decimal(bool value);
		void	set_offset(int value);
		double	get_res(const char *operation);
		void	test_function(const char *in, const char *out);
		void	set_test_type(const char *str);
		void	init_test(const char *test_type, int base, bool decimal, int offset);

	private:
		int			base_;
		int			offset_;
		bool		is_decimal_;
		FILE		*fp_;
		std::string	test_type_;
};

#endif
