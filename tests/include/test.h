#ifndef TEST_H
# define TEST_H

#include <iostream>
#include <string>

#include <stdio.h>
#include <float.h>
#include <stdio.h>

#include "../../include/class_calc.h"
#include "../../include/calculator.h"
#include "class_test.h"

void	test_conversion(Test& t);
void	test_simple_calculation(Test& t);
double	get_res(const char *operation);
void	test_function(const char *in, const char *out);

#endif
