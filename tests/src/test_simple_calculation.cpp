#include "test.h"

void	test_simple_calculation(Test& t)
{
// Integer calculation tests
	t.init_test("test simple calculation", 10, true, 0);
	t.test_function("3 + 3", "6");
	t.test_function("0 + 0", "0");
	t.test_function("5 + 0", "5");
	t.test_function("3 + 2 * 2", "7");
	t.test_function("3 - 5", "-2");
	t.test_function("3 - 5 + 7", "5");
	t.test_function("3 - 5 * 3", "-12");
	t.test_function("3 * 7 + 6 * 5", "51");
	t.test_function("3 * 7 + 9", "30");
	t.test_function("9 + 19", "28");
	t.test_function("3 + 2 - 5 * 7 + 18 - 3 * 2 + 4 + 4 * 4 - 2", "0");
	t.test_function("7 / 7", "1");
	t.test_function("8 / 2", "4");
	t.test_function("30 / 3 / 5", "2");
// Float calculation tests
	t.test_function("3.0 + 7.0", "10");
	t.test_function("3.5 - 0.5", "3");
	t.test_function("2.525 - 0.5", "2.025");
	t.test_function("3.173 + 6.827", "10");
	t.test_function("1.53 - 2.5", "-0.97");
	t.test_function("0.75 * 2.9 + 28.2 * 0.05 + 7 / 3.5", "5.585");
}
