#include "class_test.h"
#include "test.h"

Test::Test() :	base_(0),
				offset_(0),
				is_decimal_(true),
				fp_(fopen("test.log", "w")),
				test_type_("")
{

}

Test::~Test()
{
	if (fp_)
		fclose(fp_);
}

double	Test::get_res(const char *operation)
{
	Calc calc;

	calc.set_equation(operation);
	std::string res = calc.solve();
	return (calc.get_number(res.c_str()));
}

void	Test::test_function(const char *in, const char *out)
{
	double	res;
	double	sub;

	res = get_res(in);
	if (is_decimal_)
	{
		sub = std::atof(out) - res;
		if (sub > FLT_EPSILON || sub < -FLT_EPSILON)
			fprintf(fp_, "%s FAIL expected : %s output : %f\n",
				test_type_.c_str(), in, res);
	}
	else if (std::atoi(out) != (int)res)
			fprintf(fp_, "%s FAIL expected : %f output : %f\n",
				test_type_.c_str(), (float)std::stoi(std::string(in + offset_), nullptr, base_), res);
}

void	Test::set_offset(int value)
{
	offset_ = value;
}

void	Test::set_base(int value)
{
	base_ = value;
}

void	Test::is_decimal(bool value)
{
	is_decimal_ = value;
}

void	Test::set_test_type(const char *str)
{
	test_type_ = std::string(str);
}

void	Test::init_test(const char *test_type, int base, bool decimal, int offset)
{
	set_test_type(test_type);
	set_base(base);
	is_decimal(decimal);
	set_offset(offset);
}
