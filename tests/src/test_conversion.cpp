#include "test.h"
#include "class_test.h"

void	test_conversion(Test& t)
{
// Decimal integer tests
	t.init_test("conversion decimal integer tests", 10, true, 0);
	t.test_function("0", "0");
	t.test_function("128", "128");
	t.test_function("-0", "0");
	t.test_function("+0", "0");
	t.test_function("-127", "-127");
	t.test_function("+43", "43");
	t.test_function("777", "777");
	t.test_function("2147483647", "2147483647");
	t.test_function("-2147483648", "-2147483648");
// Decimal float tests
	t.init_test("conversion decimal float tests", 10, true, 0);
	t.test_function("0.000", "0.000");
	t.test_function("1.0000", "1.0000");
	t.test_function("170.0", "170.0");
	t.test_function("0.005", "0.005");
	t.test_function("143.137", "143.137");
	t.test_function("2147483646.9", "2147483646.9");
	t.test_function("-2147483647.9876", "-2147483647.9876");
// Hexadecimal tests
	t.init_test("conversion hexadecimal tests", 16, false, 2);
	t.test_function("0x1", "1");
	t.test_function("0x001", "1");
	t.test_function("0x0", "0");
	t.test_function("0x37", "55");
	t.test_function("0xfeed", "65261");
	t.test_function("0xface", "64206");
	t.test_function("0x78ec4", "495300");
	t.test_function("0xAC47e", "705662");
// Octal tests
	t.init_test("conversion octal tests", 8, false, 1);
	t.test_function("07642", "4002");
	t.test_function("00", "0");
	t.test_function("01", "1");
	t.test_function("011", "9");
	t.test_function("021", "17");
	t.test_function("01101", "577");
	t.test_function("0777", "511");
	t.test_function("0666", "438");
// Binary tests
	t.init_test("conversion binary tests", 2, false, 2);
	t.test_function("0b0", "0");
	t.test_function("0b1", "1");
	t.test_function("0b11011101", "221");
	t.test_function("0b0000", "0");
	t.test_function("0b1111111", "127");
	t.test_function("0b1000000", "64");
	t.test_function("0b111111111111111111", "262143");
}
