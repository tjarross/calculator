#include "test.h"
#include "class_test.h"

int main(void)
{
	Test *t = new Test();

	test_conversion(*t);
	test_simple_calculation(*t);
//	test_one_pair_parenthesis(*t);
//	test_multiple_parenthesis(*t);
	return (0);
}
