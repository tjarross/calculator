# Calculator v0.1 #

Calculator is a console program that allow you to make calculations. It is developed in `C++` language.

## User Dependencies ##

* Linux or Macos platform

## Features ##

* Simple calculs
* Multiple Base interpretation : 16 (prefix=0x), 10, 8 (prefix=0), 2 (prefix=0b)
* Result printed in all bases cited above, scientific format included
* Parenthesis and square brackets management (recursively)
* Operators supported (\*\*, \*, /, %, +, -, <<, >>, <, <=, >, >=, ==, !=, &, ^, |, &&, ||)
    * respecting the C and C++ operators priority
* Multiple arguments in parameter
* Floating point operations (only base 10 support floats)

### Ongoing Features ###

* Checkl it directly in file `TODO_LIST`

## Typical Usage ##

1. clone the repository
2. go to the created folder
3. `make`
4. `make test` (if you want to test the program on your platform)

### Console Mode

* Launch the resulting executable without arguments :
```
#!sh
./calculator
```
* And now enjoy the console mode ;)

### Arguments mode

* Launch the resulting executable wit arguments :
```
#!sh
./calculator "<arg1>" "<arg2>" [...] "<argN>"
```
* And now you have your results ;)

## Author ##

* Thomas JARROSSAY <tjarross@student.42.fr>

## Changelog ##

### v0.1 ###

* Add: Multiple base interpretation
* Add: Result printed in differents bases
* Add: Parenthesis and square brackets managements
* Add: Operators managements (listed above in Features)
* Add: Argument mode
* Add: Console mode
* Add: Float calculations

# Known Problems :

* Not enough tests
* Problems or crash if operation is not well formatted (no operation check for validity)